
import random

def create_diamond_deck():
  """Creates a deck containing only diamond cards for auction and shuffles it."""
  ranks = ["2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K", "A"]
  deck = []
  for rank in ranks:
    deck.append(rank)  # Include only rank for diamond cards
  random.shuffle(deck)  # Shuffle the deck before returning
  return deck

def create_card_sets():
  """Creates 3 sets of cards (excluding diamonds) representing player decks."""
  ranks = ["2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K", "A"]
  card_sets = []
  for _ in range(3):
    set_cards = []
    for rank in ranks:
      set_cards.append(rank)  # Include only rank for cards (no suits)
    card_sets.append(set_cards)
  return card_sets

def deal_hands(card_sets, num_players):
  """Deals cards to players by randomly choosing a set for each. Discards unused sets."""
  random.shuffle(card_sets)  # Shuffle sets before dealing
  hands = card_sets[:num_players]  # Take the first N sets for N players
  remaining_diamonds = create_diamond_deck()
  return hands, remaining_diamonds

def get_card_value(card):
  """Returns the point value of a card."""
  rank = card  # Rank is already extracted (no suit information)
  values = {"2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7, "8": 8, "9": 9,
            "T": 10, "J": 11, "Q": 12, "K": 13, "A": 14}
  return values[rank]

def get_human_bid(hand, current_diamond):
  """Gets a valid bid from a human player, showing the current diamond."""
  print(f"Current Diamond to be Bid On: {current_diamond}")  # Show revealed diamond
  while True:
    bid = input("Enter your bid (card, e.g., 7, KD): ")
    if bid in hand:
      return bid
    else:
      print("Invalid bid. Please enter a card from your hand (e.g., 7, KD).")

def get_computer_bid(current_diamond_value, player_hand_values, min_bid_threshold, max_bid_threshold, remaining_diamonds):
  """Simulates a bidding strategy for the computer player based on the revealed diamond value."""
  average_hand_value = sum(get_card_value(card) for card in player_hand_values) / len(player_hand_values)

  # Sacrifice bid for low-value diamonds
  if current_diamond_value < average_hand_value * min_bid_threshold:
    return min(player_hand_values, key=get_card_value)

  # Aggressive bid for high-value diamonds
  elif current_diamond_value > average_hand_value * max_bid_threshold:
    return max(player_hand_values, key=get_card_value)

  # Conditional bid based on remaining diamonds (adjust based on complexity)
  else:
    remaining_high_diamonds = sum(1 for diamond in remaining_diamonds if get_card_value(diamond) > current_diamond_value)
    if remaining_high_diamonds > len(player_hand_values) // 2:
      # More high-value diamonds remain, bid more aggressively
      return max(player_hand_values, key=lambda card: get_card_value(card) // 2)
    else:
      return min(player_hand_values, key=get_card_value)

def determine_winner(bids, point_values):
  """Finds the winner(s) of the auction based on highest bid value, handling ties."""
  highest_value = max(point_values[bid] for bid in bids)  # Find the highest point value from bids
  winners = [i for i, bid in enumerate(bids) if point_values[bid] == highest_value]  # Get indices of players with the highest bid
  return winners
def update_scores(winners, diamond_value, player_scores):
  """Updates player scores based on the winners and diamond value, splitting in case of a tie."""
  if len(winners) > 1:
    # Split points equally among winners
    points_per_winner = diamond_value / len(winners)
    for winner in winners:
      player_scores[winner] += points_per_winner
  else:
    # Single winner gets all points
    player_scores[winners[0]] += diamond_value

def play_game(num_players=2, min_bid_threshold=0.2, max_bid_threshold=0.8):
  """Runs a single game of Diamonds, showing scores after each round."""
  card_sets = create_card_sets()
  hands, remaining_diamonds = deal_hands(card_sets, num_players)
  point_values = {"2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7, "8": 8, "9": 9,
                  "T": 10, "J": 11, "Q": 12, "K": 13, "A": 14}
  player_scores = {i: 0 for i in range(num_players)}  # Initialize player scores

  # Game loop (play a round for each diamond)
  for current_diamond in remaining_diamonds:
    current_diamond_value = get_card_value(current_diamond)  # Get the point value of the diamond

    # Bidding phase
    bids = []  # Empty list to store player bids
    for player in range(num_players):
      if player == 0:
        bid = get_human_bid(hands[player], current_diamond)  # Human player bids
        print(f"Human Player bid: {bid}")  # Show human player's bid
      else:
        bid = get_computer_bid(current_diamond_value, hands[player], min_bid_threshold, max_bid_threshold, remaining_diamonds.copy())  # Computer player bids
        print(f"Computer Player {player+1} bid: {bid}")  # Show computer player's bid
      bids.append(bid)
      hands[player].remove(bid)  # Remove bid from player's hand

    # Determine winner(s) and update scores
    winners = determine_winner(bids, point_values)
    update_scores(winners, current_diamond_value, player_scores)

    # Print round score after updates
    print(f"** Round Score: **")
    for player, score in player_scores.items():
      print(f"  Player {player+1}: {score}")

  # Print the final scores
  print("Final Scores:")
  for player, score in player_scores.items():
    print(f"Player {player+1}: {score}")



# Start the game
if __name__ == "__main__":
  play_game()
