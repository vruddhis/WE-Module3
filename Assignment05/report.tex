\documentclass{article}
\usepackage{graphicx}
\usepackage{minted}
\usepackage{mdframed}
\usepackage{hyperref}
\usepackage[parfill]{parskip}  
\mdfsetup{frametitlealignment=\center}
\title{Developing strategies for the bidding card game `Diamonds' with GenAI}
\author{Vruddhi Shah}
\date{March 2024}							
\begin{document}

\maketitle


\section{Introduction}
This is a report on the endeavor of teaching generative AI (Gemini) the rules of a card game called Diamonds, and then asking it to develop strategies for the same.
\section{Problem Statement}
\subsection{The Game}
In the game of Diamonds, players engage in a bidding process to acquire diamond cards, each representing a certain number of points. The game is designed for 2 or 3 players.

\begin{itemize}
    \item Each player receives all 13 cards of one suit (excluding diamonds) at the beginning of the game.
    \item The diamond cards are shuffled and placed face down to form the auction deck.
\end{itemize}

The bidding process proceeds as follows:

\begin{enumerate}
\item Players take turns engaging in the auction, which entails bidding for the currently auctioned diamond card.
\item To place a bid, a player selects one card from their hand and discreetly places it face down as their bid.
\item Once all players have submitted their bids, the bids are revealed, and the player with the highest-value card secures the diamond card for that round. The cards that have been bid cannot be used later in the game.
\item In the event that multiple players bid the same highest-value card, the points associated with the diamond card are divided equally among them.
\end{enumerate}
This process repeats for a total of 13 rounds, with each round featuring a different diamond card up for auction. The game is won by the player who accumulates the highest number of points.

\subsection{The Task}
\subsubsection{Understanding the Game}
We need to ensure that the GenAI comprehensively understands the rules of the Diamonds card game. 

\subsubsection{Developing Effective Strategies}
Our aim is to ask it to develop effective strategies for playing Diamonds and asking it to code them up. 

\subsubsection{Iterative Improvement}
We need to iterate upon these strategies and refine them.

\section{Teaching GenAI the Game}

Initially, I perceived Gemini as a quick learner. However, as our discussions delved deeper into strategies and code implementations, it began to introduce made-up rules and unconventional setups.

Introducing the rules of the game to Gemini proved to be relatively straightforward. It posed insightful questions relevant to the game and summarized everything correctly.

It made mistakes like dealing cards randomly, including all 52 cards for auctioning. However, it promptly rectified these mistake after I pointed them out. 

Initially, in the \mintinline{python}{determine_winner} function, it simply returned the first player with the maximum number of points. However, after I pointed out that it overlooked tie scenarios, it began randomly selecting a winner from among all players with the same number of points in a given round, even though during its initial summary, it had correctly stated that points should be distributed among all winners.

Then, in one of the codes it generated for a strategy, it had a very peculiar line.
\begin{mdframed}
\begin{minted}[breaklines]{python}
if player < num_players - (num_players - sum(1 for player in hands if len(player) > 0))):
        # Human player bids
        \end{minted}
        \end{mdframed}
When I asked it to clarify, \hyperref[humanplayerlogic]{it said}:
\begin{mdframed}
\begin{minted}[breaklines]{text}
The line if player < num_players - (num_players - sum(1 for player in hands if len(player) > 0))) checks if the current player (player) is a human player in the Diamonds Game code.
        \end{minted}
        \end{mdframed}
When I questioned about if this was the best method, \hyperref[humansvscomputers]{it answered}:
\begin{mdframed}
\begin{minted}[breaklines]{text}
In the Diamonds Game code we've been building, the assumption was made that a player who runs out of cards cannot participate further and is therefore presumed to be a computer. 
        \end{minted}
        \end{mdframed}


At this point, I asked it to summarize the game to me again. Gemini had turned Diamonds into a game resembling Uno, played with cards that have flags and animals on their faces. The full answer can be found \hyperref[summary]{here}.

I explained the game again and everything went well for a while. It even gave some adequate codes. Then, for a strategy, we tried calculating the probability of a player winning in a particular round. During this, it mentioned
\begin{mdframed}
\begin{minted}[breaklines]{text}
# Probability of not winning (assuming worst-case): all other players have the same diamond
  probability_losing = num_players - 1
        \end{minted}
        \end{mdframed}
I asked it to summarize the game to me once again, and it gave me a summary of a completely different card game where players are dealt 10 cards each. The full answer can be found  \hyperref[summary2]{here}

Then, I explained the game to it again. It kept insisting on introducing new rules such as passing cards and including the points of the cards still in the player’s hands. Even after I explained that there are 13 rounds and players must bid one card in each of the rounds, it wouldn’t let go of the idea. At this point, I concluded our chat.

\section{Strategies and Code}

Initially, Gemini suggested a lot of good strategies like bluffing, forming alliances with other players, keeping track of our opponents’ cards, balancing risks, et cetera. It also differentiated between strategies for 2 player games and 3 player games.

When tasked with developing a bidding strategy, Gemini proposed a method based on comparing the average point value of the player's remaining cards with the value of the diamond being auctioned. If the diamond's value exceeded a specified bid threshold, Gemini recommended bidding the lowest valued card from the player's hand, or choosing not to bid if the threshold was not met. This approach, termed the 'Scarcity Heuristic', aimed to balance bidding aggressiveness.
Upon clarification that all players must bid in each round, Gemini updated its bidding function to include two threshold values: a lower and a higher threshold. If the diamond's value fell below the lower threshold, Gemini suggested sacrificing the minimum valued card in the player's hand. Conversely, if the diamond's value surpassed the higher threshold, Gemini advised bidding the highest valued card available. 
This code can be found \hyperref[code1]{here}.

We iterated upon the strategy and arrived at the computer using different strategies for different phases of the game. This code can be found \hyperref[code2]{here}.

It did not give me a code when I prompted it to consider opponent history (its own idea initially).

When I asked it to develop another strategy, it tried to calculate the probability of winning the round, but began to introduce made-up rules, as mentioned earlier. This code can be found \hyperref[bad]{here}. Our conversation concluded here.

\section{Analysis and Conclusions}

\begin{itemize}
    \item Gemini was good at summarizing information immediately after I had given it.
     \item Gemini demonstrated a tendency to become confused on previously established topics over time.
    \item Despite suggesting numerous strategies, when tasked with implementation, Gemini opted to focus only on the `easier' ones.
    \item Examples helped a lot in making it understand a concept.
    \item It struggled with putting together two perfectly good functions in a code.
\end{itemize}

In conclusion, while generative AI demonstrates remarkable ability to absorb and summarize information quickly, it often struggles with retaining clarity over extended interactions and translating proposed strategies into practical code.

\appendix
\section{Appendix}
The entire chat can be found \href{https://g.co/gemini/share/32698fabda3b}{here} .
\subsection{The first working code}
\label{code1}
\begin{mdframed}
\inputminted[breaklines,linenos,frame=lines]{python}{geminicode.py}
\end{mdframed}
\subsection{The modified strategy}
\label{code2}
\begin{mdframed}
\inputminted[breaklines,linenos,frame=lines]{python}{gemini2.py}
\end{mdframed}
\subsection{Function to calculate probability}
\label{bad}
\begin{mdframed}
\inputminted[breaklines,linenos,frame=lines]{python}{badgemini.py}
\end{mdframed}
\subsection{Gemini’s answer to identifying human players}
\label{humanplayerlogic}
\begin{mdframed}
\inputminted[breaklines,linenos,frame=lines]{text}{humanplayerlogic.txt}
\end{mdframed}
\subsection{Gemini’s alternative way to identify human players}
\label{humansvscomputers}
\begin{mdframed}
\inputminted[breaklines,linenos,frame=lines]{text}{humansvscomputers.txt}
\end{mdframed}
\subsection{Gemini’s horrible summary of the game}
\label{summary}
\begin{mdframed}
\inputminted[breaklines,linenos,frame=lines]{text}{summary.txt}
\end{mdframed}
\subsection{Another bad summary by Gemini}
\label{summary2}
\begin{mdframed}
\inputminted[breaklines,linenos,frame=lines]{text}{summary2.txt}
\end{mdframed}










\end{document}  