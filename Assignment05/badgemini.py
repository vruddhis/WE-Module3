def get_probability_winning(current_diamond, player_hands, num_players):
  """
  Calculates the exact probability of winning the current diamond based on known player cards.

  This approach assumes perfect knowledge of all players' remaining cards.

  Args:
      current_diamond: The value of the diamond currently being bid on (e.g., "7", "K").
      player_hands: A list of lists containing the remaining cards (excluding diamonds) for each player.
      num_players: The number of players still in the game.

  Returns:
      A float representing the probability (between 0 and 1) of winning the current diamond.
  """

  # Check if any player has the current diamond
  has_diamond = any(current_diamond in hand for hand in player_hands)

  if not has_diamond:
    # Nobody has the diamond, so the current player automatically wins
    return 1
  else:
    # Count the occurrences of the current diamond value in all player hands (excluding current player)
    total_diamond_count = 0
    for hand in player_hands[1:]:  # Skip the current player's hand
      total_diamond_count += hand.count(current_diamond)  # Count diamonds in each hand

    # Probability of winning: 1 minus the probability of losing
    probability_winning = 1 - (total_diamond_count) / (sum(len(hand) for hand in player_hands[1:]) - 1)

    return probability_winning
